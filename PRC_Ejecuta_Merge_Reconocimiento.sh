#!/bin/bash
# ************************************************************************
#  Nombre                             : PRC_Ejecuta_Merge_Reconocimiento.sh
#  Fecha Creacion       : 01/09/2015
#  Fecha Modificacion    : 01/09/2015
#  Descripcion                        : Realiza la ejecucion del Job Seq_Principal_Merge
#  Parametros                         : No requiere
#  Desarrollado                       : Everis
#  Mail                                           : Se ejecutara dentro del Job -> Seq_Principal_Merge
# ************************************************************************
. /dsapp/DSBUCREP/CNF/DSBUCREP_globales.sh

clear
# ---------------------------------------------------------------------
FECHA_HORA=`date +'%Y%m%d'` #Fecha de Ejecucion
FECHA_LOG=`date "+%Y%m%d_%H%M%S"` #Fecha de procesamiento del Log
NOM_LOG='PRC_Ejecuta_Merge_Reconocimiento' #Variable Log
_ARCHIVO_LOG=$DIRECT_LOG/${NOM_LOG}_${FECHA_LOG}.log #Nombre del archivo Log

ARCHIVOSLOG=PRC_Ejecuta_Merge_Reconocimiento*.log
ARCHIVOSDEL=PRC_Ejecuta_Merge_Reconocimiento*.txt
ARCHIVOGZ=PRC_Ejecuta_Merge_Reconocimiento*.gz

FILEMAIL_STATUS='PRC_Ejecuta_Merge_Reconocimiento';
SUBJET_MAIL_OK='Finaliza_OK_PRC_Ejecuta_Merge_Reconocimiento';
SUBJET_MAIL_ERROR='El proceso PRC_Ejecuta_Merge_Reconocimiento con ERROR';
FILE_PARTYS='PRC_Ejecuta_Merge_Reconocimiento.txt';



#----------Job a ejecutar -----------------#
DSJOB=Seq_Principal_Merge                       
export DSJOB;

# ---------------------------------------------------------------------
# Proceso Principal
echo "                                                           ">>$_ARCHIVO_LOG
echo " LAN                                                       ">>$_ARCHIVO_LOG
echo " ${NOM_LOG}                                                                    ">>$_ARCHIVO_LOG
echo `date +'%Y-%m-%d %H:%M:%S'`                                                                  >>$_ARCHIVO_LOG
echo " ----------------------------                              ">>$_ARCHIVO_LOG
echo "                                                           ">>$_ARCHIVO_LOG
echo "                                                           ">>$_ARCHIVO_LOG
echo "       ==================================================  ">>$_ARCHIVO_LOG
echo "       ********       INICIANDO EJECUCION        ********  ">>$_ARCHIVO_LOG
echo "       ********                                  ********  ">>$_ARCHIVO_LOG
echo "       ==================================================  ">>$_ARCHIVO_LOG
echo "                                                           ">>$_ARCHIVO_LOG
echo "                                                           ">>$_ARCHIVO_LOG
echo "                                                           ">>$_ARCHIVO_LOG

FECHA_PROCESO=`date "+%Y%m%d"`
FECHA_EJECUCION=`date "+%Y-%m-%d %T"`


######################
# Funciones Email #
######################

## Funcion envia Mail en caso de error #
doMailError()
{
echo `date +'%Y-%m-%d %T'`" Inicio envio mail de error " >> $_ARCHIVO_LOG

cat <<EOF >$DIRECT_TMP/$FILEMAIL_STATUS
From: $FROM_MAIL_REPORTES_BUC <Servidor BUC>
To: $LISTA_CORREO_DESDUPLICAR_PARTY
Cc:
Subject: $SUBJET_MAIL_ERROR  $FECHA_EJECUCION

Error en el Proceso Datastage PRC_Ejecuta_Merge_Reconocimiento. Ver Log

EOF
cat $DIRECT_TMP/$FILEMAIL_STATUS |/usr/lib/sendmail -t

echo `date +'%Y-%m-%d %T'`" Fin envio mail de error " >> $_ARCHIVO_LOG

}


## Funcion envia Mail OK #
#
doMailOK()
{
echo `date +'%Y-%m-%d %T'`" Inicio envio mail de ok " >> $_ARCHIVO_LOG

cat <<EOF >$DIRECT_TMP/$FILEMAIL_STATUS
From: $FROM_MAIL_REPORTES_BUC <Servidor BUC>
To: $LISTA_CORREO_DESDUPLICAR_PARTY
Cc:
Subject: $SUBJET_MAIL_OK  $FECHA_EJECUCION

El Proceso Datastage PRC_Ejecuta_Merge_Reconocimiento termino OK. Ver Log

EOF
cat $DIRECT_TMP/$FILEMAIL_STATUS |/usr/lib/sendmail -t

echo `date +'%Y-%m-%d %T'`" Fin envio mail de ok " >> $_ARCHIVO_LOG

}
#
## Borrado de LOG de + de 7 dias y archivos Temporales utilizados por los jobs
doEliminaLogTmp()
{
	echo " Proceso Eliminacion Archivos LOG/Temporales" >>$_ARCHIVO_LOG
	cd $DIRECT_LOG
	find $_PROCESO*.log -mtime +7 -print -exec rm -f {} \; >>$_ARCHIVO_LOG
	rm -f ARCHIVOSLOG

	#Elimina archivos temporales.

	cd $DIRECT_TMP
	rm -f $ARCHIVOSDEL
  
	cd $DIRECT_SEQ
	find $ARCHIVOGZ -mtime +1 -print -exec rm -f {} \;
	  
}

######################
# Ejecucion de JOB #
######################


	echo '--> ['`date +%H:%M:%S`']: ' "Inicio"                       >> $_ARCHIVO_LOG
	echo '--> ['`date +%H:%M:%S`']: ' "Reseteando  ...$DSJOB"        >> $_ARCHIVO_LOG
	$PATH_DS/dsjob -run -mode RESET $DSPROJECT $DSJOB                >> $_ARCHIVO_LOG
	echo "Ejecutando Job PRC_Ejecuta_Merge_Reconocimiento ...$DSJOB" >> $_ARCHIVO_LOG
	$PATH_DS/dsjob  -run -mode NORMAL -wait -jobstatus $DSPROJECT $DSJOB >> $_ARCHIVO_LOG
	_RETURN=$?
	echo '--> ['`date +%H:%M:%S`']:' "Proceso Concluido"              >> $_ARCHIVO_LOG
	 
	 
	#Verifica si Existe Error
	if [ $_RETURN -eq 1 ]
	then
		echo '--> ['`date +%H:%M:%S`']:' " Proceso PRC_Ejecuta_Merge_Reconocimiento Exitoso"  >> $_ARCHIVO_LOG
		echo " Proceso PRC_Ejecuta_Merge_Reconocimiento DataStage Exitoso."
		doMailOK
		cd $DIRECT_SHELL
		_FECHA_HORA=`date +'%Y%m%d_%H%M%S'`
		echo "Finaliza el proceso PRC_Ejecuta_Merge_Reconocimiento " $_FECHA_HORA >> $_ARCHIVO_LOG
		doEliminaLogTmp
		exit 0
	else
		echo '--> ['`date +%H:%M:%S`']:' " ERROR En Proceso PRC_Ejecuta_Merge_Reconocimiento. Revisar log" >> $_ARCHIVO_LOG
		echo " ERROR En Proceso PRC_Ejecuta_Merge_Reconocimiento DataStage"
		doMailError
		echo " ERROR En Proceso DataStage" >>$_ARCHIVO_LOG
		exit $_RETURN
	fi

