#====================================================================================================================================================#
# 1.- ENCABEZADO                                                                                                                                     #
# NOMBRE:             Run_Extrae_Party_Profile_SQL.sh                                                                                                #
# AUTOR:              Everis                                                                                                                         #
# FECHA CREACION:     23-11-2015                                                                                                                     #
# FECHA MODIFICACION: 23-11-2015                                                                                                                     #
# VERSION:            1.0                                                                                                                            #
# DESCRIPCION:        Extrae todos los atributos relacionados de un conjunto de party's                                                              #
# MAIL:        		  El mail lo envia el Datastage: Seq_Principal_Merge.dsx              			                                                 #
# FORMA DE EJECUCION: sh Run_Extrae_Customer_Preference.sh ${Var_ArchivoOrg}                                                                         #
# RUTA DE INSTALACION:/dsapp/DSBUCREP/SHL                                                                                                            #
# VARIABLES ENTRADA:  $1 Var_ArchivoOrg                                                                                                              #
#====================================================================================================================================================#


. /dsapp/DSBUCREP/CNF/DSBUCREP_globales.sh
date
Var_ArchivoOrg=${PATH_DAT}$1
Var_NomProc=PartyProf
Var_Archsql1=${PATH_DAT}${Var_NomProc}Exec.sql
Var_Archsql2=${PATH_DAT}${Var_NomProc}Exec2.sql
Var_Archtxt1=${PATH_DAT}${Var_NomProc}Exec.txt
Var_ArchResu=${PATH_DAT}${Var_NomProc}Result.txt

# Definicion del archivo LOG
_PROCESO=${Var_NomProc}
_FECHA_HORA=`date +'%Y%m%d_%H%M%S'`
_ARCHIVO_LOG=$PATH_LOG/SHELL_$_PROCESO\_$_FECHA_HORA.log
FECHALOG=`date +"%Y%m%d"`

rm -f ${Var_Archsql1}
rm -f ${Var_Archsql2}
rm -f ${Var_Archtxt1}
Var_ArchivoOrg=${PATH_DAT}$1
Var_ArchivoNew=${Var_ArchivoOrg}.new
echo "COMIENZO"
CANTIDAD_FILAS=`cat ${Var_ArchivoOrg}|wc -l`
cat ${Var_ArchivoOrg}|sed 's/|/,/g' > ${Var_ArchivoNew}
CONT=0;export CONT
cat ${Var_ArchivoNew}|while read LINEA
do
C1=`echo ${LINEA}|awk -F\, {'print($1)'}`
C2=`echo ${LINEA}|awk -F\, {'print($2)'}`
echo select ${C1} as c1, ${C2} as c2 from dual union all >> ${Var_Archtxt1}
done
h=`cat ${Var_Archtxt1}|wc -l`
h=`expr $h - 1`
head -$h ${Var_Archtxt1} > ${Var_Archsql1}
tail  -1 ${Var_Archtxt1}|sed 's/union all//g' >> ${Var_Archsql1}
echo "ANEXO ARCHIVOS"

echo "SELECT T1.C1||';'|| T1.C2" >> ${Var_Archsql2}
echo "||';'||NVL(PPR.PARTY_PROFILE_TYPE_CD,30)" >> ${Var_Archsql2}
echo "||';'||TO_CHAR(PPR.PARTY_PROFILE_START_DTTM,'YYYY-MM-DD HH24:MI:SS')" >> ${Var_Archsql2}
echo "||';'||PPR.PARTY_PROFILE_REASON_CD Campo1">> ${Var_Archsql2}
echo "FROM (" >> ${Var_Archsql2}
cat ${Var_Archsql1} >> ${Var_Archsql2}
echo ") T1" >> ${Var_Archsql2}
echo "JOIN PARTY_PROFILE PPR ON T1.C2 = PPR.PARTY_ID" >> ${Var_Archsql2}
echo "WHERE" >> ${Var_Archsql2} 
echo "PPR.PARTY_PROFILE_END_DTTM IS NULL" >> ${Var_Archsql2}
echo "GROUP BY " >> ${Var_Archsql2} 
echo " T1.C1" >> ${Var_Archsql2}
echo ",T1.C2" >> ${Var_Archsql2}
echo ",NVL(PPR.PARTY_PROFILE_TYPE_CD,30)" >> ${Var_Archsql2}
echo ",PARTY_PROFILE_START_DTTM" >> ${Var_Archsql2}
echo ",PPR.PARTY_PROFILE_REASON_CD" >> ${Var_Archsql2}
echo "ORDER BY 1"  >> ${Var_Archsql2}
echo ";" >> ${Var_Archsql2} 

echo "COMIENZO QUERY"

sqlplus -s ${CUENTA_ORA_BUC}<<EOF >/dev/null
SET SERVEROUTPUT ON
SET PAGESIZE 0
set linesize 30000
set colsep '|'
SET TRIMSPOOL ON
SET FEED OFF
SET TERM OFF
set serveroutput on size UNLIMITED
whenever sqlerror exit SQL.SQLCODE;
SET TERMOUT OFF;
SPOOL ${Var_ArchResu}
@${Var_Archsql2}
SPOOL OFF;
EOF

_RET=$?

rm -f ${Var_Archsql1}
rm -f ${Var_Archsql2}
rm -f ${Var_Archtxt1}

date

if [ $_RET -ne 0 ]
then
exit 1
else
exit 0
fi
