#!/bin/bash
# ************************************************************************
#  Nombre      			      : PRC_Ejecuta_Pre_Merge_Reconocimiento.sh
#  Fecha Creacion      	: 01/09/2015
#  Fecha Modificacion    : 01/09/2015
#  Descripcion 			      : Realiza la ejecucion del Job Seq_Principal_PreMerge
#  Parametros  			      : No requiere 
#  Desarrollado			      : Everis
#  Mail						  : Se ejecutara dentro del Job -> Seq_Principal_Merge
# ************************************************************************
. /dsapp/DSBUCREP/CNF/DSBUCREP_globales.sh

clear
# ---------------------------------------------------------------------
FECHA_HORA=`date +'%Y%m%d'` #Fecha de Ejecucion
FECHA_LOG=`date "+%Y%m%d_%H%M%S"` #Fecha de procesamiento del Log
NOM_LOG='PRC_Ejecuta_Merge_Reconocimiento' #Variable Log
_ARCHIVO_LOG=$DIRECT_LOG/${NOM_LOG}_${FECHA_LOG}.log #Nombre del archivo Log
NOM_ARCH=`ls ${DIRECT_SEQ}/BUC_Merge_DuplaParty_* | head -1`
#----------Job a ejecutar -----------------#
DSJOB=Seq_Principal_PreMerge		        export DSJOB;
# ---------------------------------------------------------------------
# Proceso Principal
echo "                                                           ">>$_ARCHIVO_LOG
echo " LAN                                                       ">>$_ARCHIVO_LOG
echo " ${NOM_LOG}	                 				             ">>$_ARCHIVO_LOG
echo `date +'%Y-%m-%d %H:%M:%S'` 								  >>$_ARCHIVO_LOG
echo " ----------------------------                              ">>$_ARCHIVO_LOG
echo "                                                           ">>$_ARCHIVO_LOG
echo "                                                           ">>$_ARCHIVO_LOG
echo "       ==================================================  ">>$_ARCHIVO_LOG
echo "       ********       INICIANDO EJECUCION        ********  ">>$_ARCHIVO_LOG
echo "       ********                                  ********  ">>$_ARCHIVO_LOG
echo "       ==================================================  ">>$_ARCHIVO_LOG
echo "                                                           ">>$_ARCHIVO_LOG
echo "                                                           ">>$_ARCHIVO_LOG
echo "                                                           ">>$_ARCHIVO_LOG

FECHA_PROCESO=`date "+%Y%m%d"`
FECHA_EJECUCION=`date "+%Y-%m-%d %T"`

doComprimeArchivo()
{
cd ${DIRECT_SEQ}
	NOM_ARCH_BKP=`ls BUC_Merge_DuplaParty_* | head -1`
	tar -cvf "Resp_${NOM_ARCH_BKP}".tar "${NOM_ARCH_BKP}" > /dev/null
	gzip -9 "Resp_${NOM_ARCH_BKP}.tar"
	rm "${NOM_ARCH_BKP}"
}

######################
# Ejecucion de JOB #
######################

echo "Procesando ${DSJOB} ......" >> $_ARCHIVO_LOG
echo " " >>$_ARCHIVO_LOG
echo ' --> ['`date +%H:%M:%S`']:' " Comienza ejecucion de Job "  >> $_ARCHIVO_LOG
echo "Comienza ejecucion de Job PRC_Ejecuta_Merge_Reconocimiento"
$PATH_DS/dsjob  -run -mode NORMAL -wait -jobstatus -param PL_NOMBRE_ARCHIVO="$NOM_ARCH" $DSPROJECT $DSJOB 

while true
do
  sleep 10

   $PATH_DS/dsjob -jobinfo $DSPROJECT $DSJOB >> $_ARCHIVO_LOG
  _Running=`cat $_ARCHIVO_LOG | grep "Job Status" | grep RUNNING | wc -l` >> $_ARCHIVO_LOG
  _Reset=`cat $_ARCHIVO_LOG | grep "Job Status" | grep RESET | wc -l` >> $_ARCHIVO_LOG
 
echo "Status  : " $_Running 
  if [ $_Running -ne 1 -a $_Reset -ne 1 ] 
  then 
      break; 
  fi; 
done; 
 
_Status=`cat $_ARCHIVO_LOG | grep "Job Status" | grep OK | wc -l` >> $_ARCHIVO_LOG

if [ $_Status -ne 0 ] >> $_ARCHIVO_LOG
then >> $_ARCHIVO_LOG
   echo "Status Job : $_Status" >> $_ARCHIVO_LOG
   echo "Termino Ok" >> $_ARCHIVO_LOG
   doComprimeArchivo
   exit 0 >> $_ARCHIVO_LOG
else >> $_ARCHIVO_LOG
   echo "Status Job : $_Status" >> $_ARCHIVO_LOG
   echo  "Proceso con error" >> $_ARCHIVO_LOG
   exit 2 >> $_ARCHIVO_LOG
fi; >> $_ARCHIVO_LOG




